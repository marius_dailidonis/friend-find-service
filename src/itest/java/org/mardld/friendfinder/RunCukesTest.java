package org.mardld.friendfinder;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        dryRun=false, 
        features="src/itest/resources/features", 
        glue = "org.mardld.friendfinder.stepdef",
        monochrome=true)
public class RunCukesTest {
    
}
