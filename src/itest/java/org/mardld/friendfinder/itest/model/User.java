package org.mardld.friendfinder.itest.model;

import java.util.concurrent.Future;

import io.restassured.response.Response;

public class User {

    private final String name;
    
    private Future<Response> lastFutureResponse;

    public User(String name) {
        super();
        this.name = name;
    }

    public Future<Response> getLastFutureResponse() {
        return lastFutureResponse;
    }

    public void setLastFutureResponse(Future<Response> lastFutureResponse) {
        this.lastFutureResponse = lastFutureResponse;
    }

    public String getName() {
        return name;
    }
    
}
