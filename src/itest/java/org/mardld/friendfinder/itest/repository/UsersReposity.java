package org.mardld.friendfinder.itest.repository;

import java.util.HashMap;
import java.util.Map;

import org.mardld.friendfinder.itest.model.User;

public class UsersReposity {

    private Map<String, User> users;
    
    {
        users = new HashMap<>();
    }
    
    public void add(User user) {
        users.put(user.getName(), user);
    }
    
    public User getByUserId(String userId) {
        return users.get(userId);
    }
}
