package org.mardld.friendfinder.stepdef;

import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.mardld.friendfinder.itest.model.User;
import org.mardld.friendfinder.itest.repository.UsersReposity;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;

public class CommonSteps {
    
    private Response lastResponse;
    
    private UsersReposity usersRepository = new UsersReposity();
    
    private ExecutorService executors = Executors.newFixedThreadPool(10);
    
    @When("^test operation is called$")
    public void test_operation_is_called() {
        lastResponse = get("/friend-find-service/api/v1/test");
    }

    @Then("^successful test result is returned$")
    public void successful_test_result_is_returned() {
        lastResponse.then().assertThat().statusCode(200).and().body("name", equalTo("test name"));
    }
    
    @Given("^\"([^\"]*)\" is defined as system user$")
    public void is_defined_as_system_user(String userName) {
        usersRepository.add(new User(userName));
    }

    @When("^\"([^\"]*)\" asynchronously requests to find friend$")
    public void asynchronously_requests_to_find_friend(String userName) {
        User user = usersRepository.getByUserId(userName);
        Future<Response> futureResponse = executors.submit(new Callable<Response>() {
            
            @Override
            public Response call() {
                return get("/friend-find-service/api/v1/friend?name=" + userName);
            }
        });
        
        user.setLastFutureResponse(futureResponse);
    }

    @Then("^After waiting no more than \"([^\"]*)\" seconds \"([^\"]*)\" retrieves friend named \"([^\"]*)\"$")
    public void after_waiting_no_more_than_seconds_retrieves_friend_named(String waitInSecondsAsText, String userName, String friendName) throws Exception {
        User user = usersRepository.getByUserId(userName);
        Response response = user.getLastFutureResponse().get(Long.parseLong(waitInSecondsAsText), TimeUnit.SECONDS).andReturn();
        
        response.then().assertThat().statusCode(200).and().body("name", equalTo(friendName));
    }
}
