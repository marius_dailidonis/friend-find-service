Feature: Basic User Api Operations are responsive and return predictive responses

Scenario: Test method returns a result
	When test operation is called
	Then successful test result is returned

Scenario: Two users are able to find each other
	Given "Pete" is defined as system user
	And "Laura" is defined as system user
	
	When "Pete" asynchronously requests to find friend
	And "Laura" asynchronously requests to find friend
	
	Then After waiting no more than "2" seconds "Pete" retrieves friend named "Laura"
	And After waiting no more than "2" seconds "Laura" retrieves friend named "Pete"