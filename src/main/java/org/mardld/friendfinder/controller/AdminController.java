package org.mardld.friendfinder.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@RequestMapping(value="/status", produces=MediaType.APPLICATION_JSON_VALUE)
	public StatusResponse getStatus() {
		return new StatusResponse();
	}
	
	class StatusResponse {
		
		private final String appName = "friend-find-service";

		public String getAppName() {
			return appName;
		}
		
	}
}
