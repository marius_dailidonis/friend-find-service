package org.mardld.friendfinder.controller;

import java.util.List;

import org.mardld.friendfinder.controller.async.GetFriendCallable;
import org.mardld.friendfinder.model.User;
import org.mardld.friendfinder.repository.UserRepository;
import org.mardld.friendfinder.service.FriendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 */
@RestController
@RequestMapping("/api/v1")
public class ApiController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);
    
    @Autowired
    private FriendService friendService;
    
    @Autowired
    private UserRepository userRepository;
            
    @RequestMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public FriendResponse testFriend() {
        LOGGER.info("testFriend:: ");
        User user = userRepository.findOne(1L);
        return new FriendResponse(user.getName());
    }

    @RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<User> getUsers() {
        LOGGER.info("getUsers:: ");
        return userRepository.findAll();
    }

    @RequestMapping(value = "/user", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse addUser(@RequestBody User user) {
        LOGGER.info("getUsers:: ");
        userRepository.save(user);
        return new RestResponse.Builder().withStatus("OK").build();
    }
    
    @RequestMapping(value = "/friend", produces = MediaType.APPLICATION_JSON_VALUE, params="name")
    public GetFriendCallable getFriendAsync(@RequestParam String name) {
    	return new GetFriendCallable(friendService, name, 5L);
    }
    
}

