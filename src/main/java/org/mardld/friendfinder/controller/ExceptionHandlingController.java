package org.mardld.friendfinder.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlingController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlingController.class);
    
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorInfo> handleException(Exception ex) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        LOGGER.error("handleException:: " + ex.getMessage(), ex);
        ResponseEntity<ExceptionHandlingController.ErrorInfo> response;
        
        if (ex.getClass().isAnnotationPresent(ResponseStatus.class)) {
            ResponseStatus rspStatus = ex.getClass().getDeclaredAnnotation(ResponseStatus.class);
            String message = StringUtils.isEmpty(rspStatus.reason()) ? ex.getMessage() : rspStatus.reason();
            response = new ResponseEntity<ExceptionHandlingController.ErrorInfo>(new ErrorInfo(message), headers,
                    rspStatus.value());
        } else {
            response = new ResponseEntity<ExceptionHandlingController.ErrorInfo>(new ErrorInfo(ex.getMessage()), headers,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        LOGGER.debug("handleException:: returning [{}]" + response);
        return response;
    }
    
    class ErrorInfo {
        final String message;
        
        public ErrorInfo(String message) {
            this.message = message;
        }
        
        public String getMessage() {
            return message;
        }
    }
}
