package org.mardld.friendfinder.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Friend not found in repository")
public class FriendNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5802296802293419537L;

}
