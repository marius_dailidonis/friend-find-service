package org.mardld.friendfinder.controller;

public class FriendResponse {
	
    private final String name;
    
    public FriendResponse(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

}
