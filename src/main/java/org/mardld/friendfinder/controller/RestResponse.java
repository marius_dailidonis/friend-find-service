package org.mardld.friendfinder.controller;

public class RestResponse {
    
    private String status;
    
    private RestResponse() {
        
    }

    public String getStatus() {
        return status;
    }

    private void setStatus(String status) {
        this.status = status;
    }
    
    public static class Builder {
        
        private String status;
        
        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }
        
        public RestResponse build() {
            RestResponse restResponse = new RestResponse();
            restResponse.setStatus(status);
            return restResponse;
        }
    }

}
