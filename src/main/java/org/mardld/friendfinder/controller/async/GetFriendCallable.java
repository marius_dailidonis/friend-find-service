package org.mardld.friendfinder.controller.async;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.mardld.friendfinder.controller.FriendNotFoundException;
import org.mardld.friendfinder.controller.FriendResponse;
import org.mardld.friendfinder.service.FriendService;
import org.mardld.friendfinder.service.FriendshipRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */
public class GetFriendCallable implements Callable<FriendResponse> {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(GetFriendCallable.class);
	
	private final FriendService friendService;
	
	private final String requesterName;
	
	private final long awaitTimeoutInSeconds;
	
	public GetFriendCallable(FriendService friendService, String requesterName, long awaitTimeoutInSeconds) {
		this.friendService = friendService;
		this.requesterName = requesterName;
		this.awaitTimeoutInSeconds = awaitTimeoutInSeconds;
	}

	@Override
	public FriendResponse call() throws Exception {
        String friendName = null;
        
        FriendshipRequest friendshipRequest = friendService.getFriendshipRequest();
        
        if (friendshipRequest == null) {
            LOGGER.info("name='{}' | friendship request not found.", requesterName);
            friendshipRequest = new FriendshipRequest(requesterName);
            friendService.registerFriendshopRequest(friendshipRequest);
            
            LOGGER.info("name='{}' | new friendship request registered.", requesterName);
            
            boolean received = friendshipRequest.getCountDownLatch().await(awaitTimeoutInSeconds, TimeUnit.SECONDS);
            if (!received) {
                LOGGER.error("name='{}' | failed to find a friend. Canceling request.", requesterName);
                friendService.removeFriendshipRequest(friendshipRequest);
            	throw new FriendNotFoundException();
            }
            
            friendName = friendshipRequest.getResponderName();
            LOGGER.info("name='{}' | friend found with name={}", requesterName, friendName);
            
        } else {
            
            friendshipRequest.setResponderName(requesterName);
            friendName = friendshipRequest.getRequesterName();
            LOGGER.info("name='{}' | friendship request found. friendName={}", requesterName, friendName);
            
            LOGGER.info("name='{}' | notifying friend...", requesterName, friendName);
            friendshipRequest.getCountDownLatch().countDown();
            LOGGER.info("name='{}' | notified", requesterName, friendName);
        }
        
        return new FriendResponse(friendName);
	}

	public FriendService getFriendService() {
		return friendService;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public long getAwaitTimeoutInSeconds() {
		return awaitTimeoutInSeconds;
	}

}
