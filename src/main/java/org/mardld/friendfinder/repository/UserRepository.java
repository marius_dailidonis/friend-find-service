package org.mardld.friendfinder.repository;

import org.mardld.friendfinder.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

}
