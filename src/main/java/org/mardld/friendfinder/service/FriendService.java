package org.mardld.friendfinder.service;

/**
 * 
 */
public interface FriendService {

	/**
	 * 
	 * @return
	 */
	FriendshipRequest getFriendshipRequest();
	
	/**
	 * 
	 * @param request
	 */
	void registerFriendshopRequest(FriendshipRequest request);
	
	/**
	 * 
	 * @param request
	 */
	void removeFriendshipRequest(FriendshipRequest request);
}
