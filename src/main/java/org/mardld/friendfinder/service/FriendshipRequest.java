package org.mardld.friendfinder.service;

import java.util.concurrent.CountDownLatch;

/**
 * 
 */
public class FriendshipRequest {
    
    private final String requesterName;
    
    private final CountDownLatch countDownLatch;
    
    private String responderName;
    
    public FriendshipRequest(String requesterName) {
        this(requesterName, new CountDownLatch(1), null);
    }
    
    public FriendshipRequest(String requesterName, CountDownLatch countDownLatch, String responderName) {
        this.requesterName = requesterName;
        this.countDownLatch = countDownLatch;
        this.responderName = responderName;
    }

    public synchronized void setResponderName(String responderName) {
        this.responderName = responderName;
    }
    
    public String getRequesterName() {
        return requesterName;
    }
    
    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }
    
    public synchronized String getResponderName() {
        return responderName;
    }

}
