package org.mardld.friendfinder.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class LocalMemoryFriendService implements FriendService {
    
    private static final List<FriendshipRequest> awaitingFriendList = Collections
            .synchronizedList(new ArrayList<FriendshipRequest>());

	@Override
	public FriendshipRequest getFriendshipRequest() {
        synchronized (awaitingFriendList) {
            for (Iterator<FriendshipRequest> iterator = awaitingFriendList.iterator(); iterator.hasNext();) {
                FriendshipRequest friendshipRequest = iterator.next();
                iterator.remove();
                return friendshipRequest;
            }
        }
        return null;
	}

	@Override
	public void removeFriendshipRequest(FriendshipRequest request) {
        synchronized (awaitingFriendList) {
        	awaitingFriendList.remove(request);
        }
	}

	@Override
	public void registerFriendshopRequest(FriendshipRequest request) {
        synchronized (awaitingFriendList) {
        	awaitingFriendList.add(request);
        }
	}

}
