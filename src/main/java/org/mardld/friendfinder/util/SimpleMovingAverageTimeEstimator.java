package org.mardld.friendfinder.util;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Time Estimated based on Simple Moving Average formula which result is arithmetic average of last n values.
 * More at {@link https://en.wikipedia.org/wiki/Moving_average#Simple_moving_average}
 *
 */
public class SimpleMovingAverageTimeEstimator implements TimeEstimator {
	
	final int n;
	
	final Queue<Long> data = new LinkedList<>();
	
	final ReadWriteLock rwLock = new ReentrantReadWriteLock(true);
	
	/**
	 * Constructs SimpleMovingAverageTimeEstimator with defined last inserted n data points to evaluate from. 
	 * @param n - size of max capacity of the data to be stored. Oldest entries are removed in favor of newest complying FIFO.
	 */
	public SimpleMovingAverageTimeEstimator(int n) {
		this.n = n;
	}

	@Override
	public void addDataEntry(long timeInMillis) {
		try {
			rwLock.writeLock().lock();
			data.offer(timeInMillis);
			while (data.size() > n) {
				data.poll();
			}
		} finally {
			rwLock.writeLock().unlock();
		}
	}

	@Override
	public long getEstimate() {
		long result = 0;
		try {
			rwLock.readLock().lock();
			if (data.isEmpty()) {
				return 0;
			}
			for (Long entry : data) {
				result += entry;
			}
			result /= data.size();
		} finally {
			rwLock.readLock().unlock();
		}
		return result;
	}

}
