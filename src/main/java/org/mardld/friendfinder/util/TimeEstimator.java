package org.mardld.friendfinder.util;

/**
 * 
 */
public interface TimeEstimator {

	/**
	 * Provides data into data array.
	 * @param timeInMillis
	 */
	void addDataEntry(long timeInMillis);
	
	/**
	 * Returns calculated estimate based on data array.
	 * @return
	 */
	long getEstimate();
}
