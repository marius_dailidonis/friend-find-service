package org.mardld.friendfinder;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ApplicationConfigTests {
    
    @InjectMocks
    ApplicationConfig applicationConfig;
    
    @Mock
    AsyncSupportConfigurer asyncSupportConfigurer;
    
    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void configureAsyncSupport() {
        
        // test
        applicationConfig.configureAsyncSupport(asyncSupportConfigurer);
        
        // verify
        verify(asyncSupportConfigurer).setDefaultTimeout(1800000L);
    }
}
