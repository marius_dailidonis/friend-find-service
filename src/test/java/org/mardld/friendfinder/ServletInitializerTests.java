package org.mardld.friendfinder;

import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.DispatcherServlet;
import org.testng.annotations.BeforeMethod;

public class ServletInitializerTests {
    
    @InjectMocks
    ServletInitializer initializer;
    
    @Mock
    ServletContext servletContext;
    
    @Mock
    Dynamic dynamic;
    
    @BeforeMethod
    public void beforeMethod() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void onStartup() throws ServletException {
        
        // prepare
        when(servletContext.addServlet(eq("dispatcher"), any(DispatcherServlet.class))).thenReturn(dynamic);
        
        // test
        initializer.onStartup(servletContext);
        
        // verify
        verify(dynamic).setLoadOnStartup(1);
        verify(dynamic).setAsyncSupported(true);
        verify(dynamic).addMapping("/*");
    }
}
