package org.mardld.friendfinder.controller;

import org.mardld.friendfinder.controller.async.GetFriendCallable;
import org.mardld.friendfinder.service.FriendService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ApiControllerTest {
    
	@InjectMocks
    private ApiController controller;
	
	@Mock
	private FriendService friendService;
    
    @BeforeMethod
    public void beforeMethod() {
    	MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testFriend() {
        
        // test
    	FriendResponse actual = controller.testFriend();
        
        // assert
        Assert.assertEquals(actual.getName(), "test name");
    }
    
    @Test
    public void testGetFriendAsync() {
        
    	// prepare
    	String name = "test requester name";
    	
        // test
    	GetFriendCallable actual = controller.getFriendAsync(name);
        
        // assert
        Assert.assertEquals(actual.getAwaitTimeoutInSeconds(), 5);
        Assert.assertEquals(actual.getRequesterName(), name);
        Assert.assertEquals(actual.getFriendService(), friendService);
    	
    }
}
