package org.mardld.friendfinder.controller;

import org.mardld.friendfinder.controller.ExceptionHandlingController.ErrorInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ExceptionHandlingControllerTest {

	private ExceptionHandlingController controller;
	
	static final String REASON = "test reason";
	
	@BeforeMethod
	public void beforeMethod() {
		controller = new ExceptionHandlingController();
	}
	
	@DataProvider
	public Object[][] dp() {
		String message = "some test message";
		return new Object[][] {
			{new Exception(message), HttpStatus.INTERNAL_SERVER_ERROR, message},
			{new ResponseStatusWithReasonAnotatedException(), HttpStatus.BAD_REQUEST, REASON},
			{new ResponseStatusAnotatedException(message), HttpStatus.NOT_FOUND, message},
		};
	}

	@Test(dataProvider="dp")
	public void handleException(Exception ex, HttpStatus expectedHttpStatus, String expectedMessage) {
		
		// test
		ResponseEntity<ErrorInfo> actual = controller.handleException(ex);
		
		// assert
		Assert.assertEquals(actual.getStatusCode(), expectedHttpStatus);
		Assert.assertEquals(actual.getBody().getMessage(), expectedMessage);
	}
	
	@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason=REASON)
	static class ResponseStatusWithReasonAnotatedException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4411512565801028639L;
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	static class ResponseStatusAnotatedException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4266884904184169000L;
		
		public ResponseStatusAnotatedException(String message) {
			super(message);
		}
	}
}
