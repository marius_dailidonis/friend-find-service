package org.mardld.friendfinder.controller.async;

import org.mardld.friendfinder.controller.FriendNotFoundException;
import org.mardld.friendfinder.controller.FriendResponse;
import org.mardld.friendfinder.service.FriendService;
import org.mardld.friendfinder.service.FriendshipRequest;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import java.util.concurrent.CountDownLatch;

import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GetFriendCallableTest {
	
	private GetFriendCallable callable;
	
	@Mock
	private FriendService friendService;
	
	private String requesterName = "Jonas";
	
	private long awaitTimeoutInSeconds = 1L;
	
	@BeforeMethod
	public void beforeMethod() {
		MockitoAnnotations.initMocks(this);
		callable = new GetFriendCallable(friendService, requesterName, awaitTimeoutInSeconds);
	}

	@Test
	public void call_friend_available_before() throws Exception {
		
		// prepare
		FriendshipRequest friendshipRequest = mock(FriendshipRequest.class);
		when(friendService.getFriendshipRequest()).thenReturn(friendshipRequest);
		
		CountDownLatch latch = mock(CountDownLatch.class);
		when(friendshipRequest.getCountDownLatch()).thenReturn(latch);
		
		String friendName = "Pete";
		when(friendshipRequest.getRequesterName()).thenReturn(friendName);
		
		// test
		FriendResponse actual = callable.call();
		
		// assert
		Assert.assertEquals(actual.getName(), friendName);
		
		// verify
		verify(latch).countDown();
		verifyNoMoreInteractions(latch);
	}

	@Test
	public void call_friend_available_after() throws Exception {
		
		// prepare
		String friendName = "Pete";
		
		doAnswer(new Answer<Object>() {

			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				FriendshipRequest request = invocation.getArgumentAt(0, FriendshipRequest.class);
				request.setResponderName(friendName);
				request.getCountDownLatch().countDown();
				return null;
			}
		}).when(friendService).registerFriendshopRequest(any(FriendshipRequest.class));
		
		// test
		FriendResponse actual = callable.call();
		
		// assert
		Assert.assertEquals(actual.getName(), friendName);
		
		// verify
	}

	@Test(expectedExceptions=FriendNotFoundException.class)
	public void call_no_friends_available() throws Exception {
		
		// test
		callable.call();
	}

	@Test
	public void getAwaitTimeoutInSeconds() {
		
		// test
		long actual = callable.getAwaitTimeoutInSeconds();
		
		// assert
		Assert.assertEquals(actual, 1L);
	}

	@Test
	public void getFriendService() {
		
		// test
		FriendService actual = callable.getFriendService();
		
		// assert
		Assert.assertEquals(actual, friendService);
	}

	@Test
	public void getRequesterName() {
		
		// test
		String actual = callable.getRequesterName();
		
		// assert
		Assert.assertEquals(actual, "Jonas");
	}
}
