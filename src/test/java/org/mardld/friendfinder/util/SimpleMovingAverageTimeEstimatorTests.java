package org.mardld.friendfinder.util;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SimpleMovingAverageTimeEstimatorTests {
	
	@DataProvider
	public Object[][] dp() {
		return new Object[][] {
			{new long[]{}, 10, 0 },
			{new long[]{5}, 10, 5 },
			{new long[]{5, 15}, 10, 10 },
			{new long[]{10, 20, 30}, 10, 20 },
			{new long[]{10, 20, 30}, 2, 25 },
			{new long[]{10, 20, 30}, 1, 30 },
		};
	}

	@Test(dataProvider="dp")
	public void getEstimate(long[] data, int n, long expected) {
		// prepare
		SimpleMovingAverageTimeEstimator estimator = new SimpleMovingAverageTimeEstimator(n);
		for (int i = 0; i < data.length; i++) {
			estimator.addDataEntry(data[i]);
		}
		
		// test
		long actual = estimator.getEstimate();
		
		// assert
		Assert.assertEquals(actual, expected);
	}
}
